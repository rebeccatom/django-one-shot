from django.db import models

# Create your models here.

class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    #how to make this datetime field optional
    #you put null = true and blank = true into the field
    due_date = models.DateTimeField(null=True, blank=True)
    #how to make this a boolean field or checkbox and make it default to false
    #you use default = False, and vice versa
    completed_on = models.BooleanField(default=False)
    todo_list = models.ForeignKey(TodoList, related_name="items", on_delete=models.CASCADE)
